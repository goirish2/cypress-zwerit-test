/// <reference types="Cypress" />

describe("Zwerit Web Form", function() 
{

    it('Verfify Zwerit Page Title', function() 
    {
        
        //open Zwerit URL
        cy.visit('https://zwerit.com/test/')
        
        // verify URL page title
        cy.title().should('include','Test Page for Automation')
         
    })
    
    it('Verfify Name Input & Email Input Boxes', function() 
    {
        
        //get name text input box element, add text
        cy.get('#form-field-name').should('be.visible').should('be.enabled').type('John Kerr')
        
        // verify email input box content
        cy.get('#form-field-email').should('be.visible').should('be.enabled').type('john.kerr.software@gmail.com')
         
    })

    it('Verfify Radio Button Control', function() 
    {
        
        // verify radio buttons enabled, and select one
        cy.get('#form-field-field_2362b18-1').should('be.visible').should('be.enabled').should('not.be.checked').click()
        
        // verify click selected persists
        cy.get('#form-field-field_2362b18-1').should('be.checked')
         
    })

    it('Verfify Number Selector Input', function() 
    {
        
        // verify number selector input control exists, type number
        cy.get('#form-field-field_750e579').should('be.visible').should('be.enabled').type('13')
        
        // verify number typed persists
        cy.get('#form-field-field_750e579').should('have.value','13')
         
    })

    it('Verify Checkbox Input', function() 
    {
        
        // verify checkbox input control exists, select one
        cy.get('#form-field-field_c887732-0').should('be.visible').should('not.be.checked').click()
        
        // verify checked box (selected list item) persists
        cy.get('#form-field-field_c887732-0').should('be.checked')
         
    })

    it('Verify Dropdown Input', function() 
    {
    
        // verify dropdown input control exists, select one
        cy.get('#form-field-field_979caa1').should('be.visible').select('Dropdown option 3')
        
        // verify dropdwon selection persists
        cy.get('#form-field-field_979caa1').should('have.value','Dropdown option 3')
         
    })

    it('Verify Date Selector Input & Submit Form', function() 
    {
    
        // verify date selector control exists, select one
        cy.get('#form-field-field_defe28c').should('be.visible').click()

        // select date
        cy.get('#form-field-field_defe28c').type('2021-08-31')

        // submit form using send button
        cy.get('body > div.elementor.elementor-812 > div > div > section.elementor-section.elementor-top-section.elementor-element.elementor-element-536d63c.elementor-section-boxed.elementor-section-height-default.elementor-section-height-default > div > div > div.elementor-column.elementor-col-50.elementor-top-column.elementor-element.elementor-element-6e06a5f > div > div > div.elementor-element.elementor-element-dd75ac9.elementor-button-align-stretch.elementor-widget.elementor-widget-form > div > form > div > div.elementor-field-group.elementor-column.elementor-field-type-submit.elementor-col-100.e-form__buttons > button > span > span.elementor-button-text').click({force: true})
        
        // get modal, verify correct image test
        cy.get('#elementor-popup-modal-885 > div > div.dialog-message.dialog-lightbox-message > div > div > section > div > div > div > div > div > div.elementor-element.elementor-element-3bb8d54c.elementor-widget.elementor-widget-image > div > div > img')
          .should('be.visible')

        // click outside the modal to return to form minus modal
        cy.get('body').click(0,0)  

        // add other dummy buttons as time allows
        
          
    })

})